﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace Updater
{
    class Updater
    {
        static void Main(string[] args)
        {
            Console.WriteLine("************************************************");
            Console.WriteLine("********* Reborn Patcher - Updater v02**********");
            Console.WriteLine("************************************************");
            Console.WriteLine(" ");

            string jmeno = "Reborn";

            try
            {
                foreach (Process pr in Process.GetProcesses())
                {
                    if ((pr.ProcessName.ToLower().Equals ("patcher") || pr.ProcessName.ToLower().Equals("reborn")) && Path.GetDirectoryName(pr.MainModule.FileName).Equals(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName)))
                    {
                        Console.WriteLine("Ukončuji Patcher (pid:"+pr.Id+")..");
                        jmeno = pr.ProcessName;
                        pr.Kill();
                        System.Threading.Thread.Sleep(1000);
                        if(File.Exists(jmeno+".exe")) File.Delete(jmeno+".exe");
                    }
                }

            }
            catch (InvalidOperationException)
            {
                //
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                WebClient wc = new WebClient();
                Console.WriteLine("Stahuji nový Patcher..");
                wc.DownloadFile("http://patcher.reborn.cz/update/Patcher.exe", "Reborn.exe");
                Console.WriteLine("Aktualizace byla úspěšně stažena!");
                Console.WriteLine("Spouštím Patcher...");
                System.Threading.Thread.Sleep(1000);
                System.Diagnostics.Process.Start("Reborn.exe", "-u");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Vyskytla se chyba: "+ex.Message);
            }
            Console.WriteLine("Aktualizace byla úspěšná!");
            System.Threading.Thread.Sleep(1000);

        }
    }
}
