using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Patcher;
using System.Diagnostics;
using System.Management;

namespace Patcher
{
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class formPatcher : Form
    {
        /// <summary>
        /// Verze Patcheru
        /// </summary>
        public const float VERSION = 2.17f;
        public const string VERSIONS = "2.17";


        private formSettings setForm = null;
        private bool aktualizuj = false;

        private delegate void BrowserCallback(String text);
        private delegate void LabelTextCallback(Label label, String text);
        private delegate void LabelColorCallback(Label label, Color color);
        private delegate void StatusTextCallback(String text);
        private delegate void DownloadProgressCallback(int read, int total, int speed);
        private delegate void EnableButtonCallback(Button button, bool enabled);
        private delegate void UpdateProgressCallback(int value, int maximum);
        private delegate void UpdateCheckCallback(bool aktualizuj);
        private delegate void DoSchrankyCallback(string text);

        struct PatchInfo
        {
            public int num, type;
            public string patch_file, dest_file, type_str;
        }
       
        public const int
            PATCH_INVALID = -1,
            PATCH_REWRITE = 1,
            PATCH_DELETE = 2,
            PATCH_PATCH = 3,
            PATCH_TEXT = 4,
            PATCH_REMOVE = 5; //zmaze subor/adresar v GRF

        public const int
            SET_EFF = 0,
            SET_PETS = 1,
            SET_MF = 2,
            SET_HKCU = 3,
            SET_ANTISHAKE = 4,
            SET_SCREEN = 5;

        public const int
            SET_EFF_STANDARD = 0,
            SET_EFF_MINIMAL = 1,
            SET_EFF_GW = 2,

            SET_PETS_CZECH = 0,
            SET_PETS_OFF = 1,

            SET_MF_EXT = 0,
            SET_MF_EXE = 1,
            SET_MF_DLL = 2,

            SET_HKCU_HKLM = 0,
            SET_HKCU_HKCU = 1,

            SET_SCREEN_WINDOW = 0,
            SET_SCREEN_FULLSCREEN = 1,

            SET_ANTISHAKE_OFF = 0,
            SET_ANTISHAKE_ON = 1;

        public Thread ThrMain;

        public string root = "http://patcher.reborn.cz/";
        public string patchdir = "RebornPatch\\";

        private const int SETTINGS_COUNT = 6; // pocet nastaveni
        byte[] settings = new byte[SETTINGS_COUNT]; // 6 moznosti nastaveni

        public WebClient wc = new WebClient();
        private IWebProxy proxy;

        // inicializace
        public formPatcher()
        {
            InitializeComponent();
            this.Text = "Reborn Patcher " + VERSIONS + " - (c) 2009-2016 k3dT, Koca & Baalberith";
            wc.BaseAddress = root;
            wc.Proxy = null;
            proxy = WebRequest.DefaultWebProxy;

        }

        public void OpenURL(String url)
        {
            Process.Start(url);
        }

        /// <summary>
        /// hook na enter a escape klavesu
        /// </summary>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            try
            {
                // esc = zavrit patcher
                if (msg.WParam.ToInt32() == (int)Keys.Escape)
                {
                    ThrMain.Abort();
                    Application.Exit();
                }
                else
                // enter = spustit hru
                if (msg.WParam.ToInt32() == (int)Keys.Enter)
                {
                       if (btnStart.Enabled == true) btnStart_Click(null,null);
                }
                else
                {
                    return base.ProcessCmdKey(ref msg, keyData);
                }
            }
            catch (Exception Ex)
            {
                ProcessException(Ex);
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void DoSchranky(string text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new DoSchrankyCallback(DoSchranky), text);
            }
            else
            {
                try
                {
                    Clipboard.SetText(text);
                }
                catch (Exception Ex)
                {
                    ProcessException(Ex);
                }
            }
        }


        /// <summary>
        /// callback z threadu na aktualizaci patcheru
        /// </summary>
        private void UpdateCkeck(bool aktualizuj)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateCheckCallback(UpdateCkeck), aktualizuj);
            }
            else
            {
                try
                {
                    if (aktualizuj == true)
                    {
                        setStatus("Byla nalezena nová verze Reborn Patcheru!");
                    }

                    if (aktualizuj == true && (MessageBox.Show(this, "Je k dispozici nová verze Reborn Patcheru! Bude provedena aktualizace.", "Aktualizace Reborn patcheru", MessageBoxButtons.OK, MessageBoxIcon.Question) == DialogResult.OK))
                    {
                        // ukoncime nejdriv radsi patchovaci thread, kdo vi co by to udelalo...
                        if (ThrMain.IsAlive)
                            ThrMain.Abort();

                        setStatus("Stahuju aktuální updater..");

                        if (DownloadFile("update/Updater.exe", "Updater.exe") > 0)
                            throw new Exception("Nepodařilo se stáhnout updater z webu... Zkontroluj připojení k internetu a zkus to znova.");

                        if (File.Exists("Updater.exe"))
                            try
                            {
                                Process.Start("Updater.exe");
                            }
                            catch (Exception Ex)
                            {
                                throw new Exception("Nelze spustit updater! ("+Ex.Message+")");
                            }
                        else
                            throw new Exception("Chyba! Nepodařilo se stáhnout aktualizační soubor (updater.exe).");
                        return;
                    }
                } catch (Exception Ex) {
                    ProcessException(Ex);
                }
            }
        }

        /// <summary>
        /// hlavni thread celyho patchovani
        /// </summary>
        [STAThread]
        public void PatchClient()
        {
 
            EnableButton(btnSettings, true);

            MemoryStream ms = null;
            BinaryReader reader = null;
            FileStream stream = null;
            XmlTextReader xml = null;
            try
            {
                int patchnum = 0;
                try
                {
                    reader = new BinaryReader(new FileStream("rebpatch.inf", FileMode.Open, FileAccess.Read, FileShare.Read));
                    patchnum = reader.ReadInt32();
                    reader.Close();

                }
                catch (Exception)
                {
                    patchnum = 0;
                }

                setStatus("Informace staženy. Načítám nové patche..");

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select ProductName from Win32_SoundDevice");
                
                if (searcher.Get().Count == 0) 
                    MessageBox.Show("Na tvém počítači není nainstalovaná žádná zvuková karta. Spusť Setup.exe a vypni tam nastavení zvuku, jinak se hra vůbec nespustí.", "Chybí zvuková karta", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                LoadSettings();

                byte[] xmldata;

                try
                {
                    string[] args = Environment.GetCommandLineArgs();
                    int vyvoj = 0;
                
                    foreach(string arg in args){
                        int result = String.Compare(arg, "-vyvoj");
                        if (0 == result) {
                            vyvoj = 1;
                        }
                    }

                    setStatus("Stahování XML souboru s aktualizacemi...");
                    if (vyvoj == 1) {
                        xmldata = wc.DownloadData("patch-vyvoj.xml");
                    } else {
                        xmldata = wc.DownloadData("patch2.xml");
                    }                    
                    setStatus("Soubor s definicí aktualizací byl stažen.");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Nepodařilo se stáhnout soubor s aktualizacemi! Zkontroluj připojení k internetu. ("+Ex.Message+").");
                }

                MemoryStream xmlStream = new MemoryStream(xmldata);
                xml = new XmlTextReader(xmlStream);
                List<PatchInfo> patches = new List<PatchInfo>();
                int new_patches = 0;
                float verze = 0;

                while (xml.Read())
                {
                    switch (xml.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (xml.Name.ToLower())
                            {
                                case "patch_definition":
                                    while (xml.MoveToNextAttribute())
                                    {
                                        if (xml.Name.ToLower().Equals("version"))
                                        {
                                            var xmlverze = xml.Value.Replace(",", ".");
                                            if (float.TryParse(xmlverze, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out verze))
                                            {
                                                if (verze > VERSION)
                                                    aktualizuj = true;
                                            }
                                            else
                                            {
                                                throw new Exception("Chybná definice verze patcheru v XML. Kontaktuj prosím k3dT.");
                                            }
                                        }
                                    }
                                    break;
                                case "patch":
                                    PatchInfo pi = new PatchInfo();
                                    while (xml.MoveToNextAttribute())
                                    {
                                        switch (xml.Name.ToLower())
                                        {
                                            case "number":
                                                if (!int.TryParse(xml.Value, out pi.num))
                                                {
                                                    throw new Exception("Chybné číslo patche. Řádek: " + xml.LineNumber + ", sloupec: " + xml.LinePosition);

                                                }
                                                break;
                                            case "type":
                                                pi.type = typeToInt(xml.Value.ToLower());
                                                pi.type_str = typeToStr(pi.type);
                                                break;
                                            case "patch_file":
                                                pi.patch_file = xml.Value;
                                                break;
                                            case "file":
                                                pi.dest_file = xml.Value;
                                                break;

                                            default:
                                                throw new Exception("Neznámý atribut v definici patchu. Řádek: " + xml.LineNumber + "; sloupec: " + xml.LinePosition);
                                        }
                                    }

                                    if (pi.num <= 0 || (pi.dest_file == null && pi.type != PATCH_TEXT) ||
                                        ((pi.type == PATCH_REWRITE || pi.type == PATCH_PATCH) && pi.patch_file == null))
                                    {
                                        throw new Exception("Chybná definice patche. Řádek: " + xml.LineNumber);
                                    }
                                    patches.Add(pi);
                                    if (pi.num > patchnum) new_patches++;

                                    break;
                            }
                            break;
                    }

                }

                UpdateProgress(100, 100);
                xml.Close();


                browser.Navigate("http://patcher.reborn.cz/index.php");

                UpdateCkeck(aktualizuj);


                if (new_patches == 0)
                {
                    //pokial ma uzivatel custom patch subor:
                    CustomPatche();
                    setStatus("Žádné nové patche k dispozici. Můžeš spustit hru!");
                    EnableButton(btnStart, true);
                    EnableButton(btnReplay, true);
                    return;
                }

                FileStream tmp_stream;
                try
                {
                    tmp_stream = new FileStream("reborn.grf", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                    tmp_stream.Close();

                    tmp_stream = new FileStream("palette.grf", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                    tmp_stream.Close();
                }
                catch (Exception)
                {
                    EnableButton(btnStart, true);
                    EnableButton(btnReplay, true);
                    EnableButton(btnClose, true);
                    EnableButton(btnSettings, true);
                    throw new Exception("Máš spuštěný Reborn! Pokud ho chceš opatchovat, musíš nejdřív vypnout hru a znova pustit patcher!<br/><br/>(Nelze otevřít soubory palety.grf a reborn.grf pro zápis)");
                }

                EnableButton(btnStart, false);
                EnableButton(btnReplay, false);
                EnableButton(btnClose, false);
                EnableButton(btnSettings, false);

                foreach (PatchInfo pi in patches)
                {
                    if (pi.num <= patchnum) continue;
                    switch (pi.type)
                    {
                        case PATCH_DELETE:
                            if (File.Exists(pi.dest_file))
                                try
                                {
                                    File.Delete(pi.dest_file);
                                }
                                catch
                                {
                                    try
                                    {
                                        File.Move(pi.dest_file, pi.dest_file + ".old");
                                    }
                                    catch
                                    {
                                        throw new Exception("Nelze smazat ani přejmenovat soubor '" + pi.dest_file + "'. Restartuj počítač, a zkus to znova.");
                                    }
                                }
                            break;

                        case PATCH_REWRITE:
                            string downfile = pi.patch_file;
                            string savefile;
                            if (pi.patch_file.StartsWith("http"))
                                savefile = pi.patch_file.Substring(pi.patch_file.LastIndexOf('/'));
                            else
	                            savefile = pi.patch_file;
                            if (DownloadFile(pi.patch_file, patchdir + savefile) > 0)
                                    throw new Exception("Chyba při stahování souboru '" + pi.patch_file + "'");

                            if (!File.Exists(patchdir + savefile))
                                throw new Exception("Nenalezen soubor '" + patchdir + savefile + "' - asi se nestáhl správně.");

                            if (File.Exists(pi.dest_file))
                                try
                                {
                                    File.Delete(pi.dest_file);
                                }
                                catch
                                {
                                    throw new Exception("Nelze smazat soubor '" + pi.dest_file + "'. Restartuj počítač, a zkus to znova.");
                                }

                            try
                            {
                                File.Copy(patchdir + savefile, pi.dest_file);
                            }
                            catch
                            {
                                throw new Exception("Nelze zkopírovat soubor '" + patchdir + savefile + "' na soubor '" + pi.dest_file + "'. Restartuj počítač, a zkus to znova.");
                            }

                            // pokud vyjde novej reexe a hrac ma zapnutej antishake, patchneme stazenej soubor
                            if (pi.dest_file.Equals("reexe.exe") && settings[SET_ANTISHAKE] == SET_ANTISHAKE_ON)
                            {
                                setAntishake(true);
                            }

                            // pokud je v patchi reexe.exe nebo setup.exe zkontrolujeme jestli je zapnuty HKCU, pripadne patchnem 
                            if (pi.dest_file.Equals("reexe.exe") && settings[SET_HKCU] == SET_HKCU_HKCU)
                                registryPatch("reexe.exe", 0x02, 0x01);

                            if (pi.dest_file.Equals("setup.exe") && settings[SET_HKCU] == SET_HKCU_HKCU)
                                registryPatch("setup.exe", 0x02, 0x01);
                            break;

                        case PATCH_PATCH:
                            if (DownloadFile(pi.patch_file, patchdir + pi.patch_file) > 1)
                                throw new Exception("Chyba při stahování souboru '" + pi.patch_file + "'");


                            if (!File.Exists(patchdir + pi.patch_file))
                                throw new Exception("Chybí soubor '" + patchdir + pi.patch_file + "' - asi se nestáhl správně.");

                            if (!File.Exists(pi.dest_file))
                                try
                                {
                                    File.Copy(patchdir + pi.patch_file, pi.dest_file);
                                }
                                catch
                                {
                                    throw new Exception("Nelze zkopírovat soubor '" + patchdir + pi.patch_file + "' na soubor '" + pi.dest_file + "'. Restartuj počítač, a zkus to znova.");
                                }

                            try
                            {
                                PatchFile(patchdir + pi.patch_file, pi.dest_file);
                            }
                            catch
                            {
                                throw new Exception("Nelze spojit GRF soubor '" + patchdir + pi.patch_file + "' do '" + pi.dest_file + "'");
                            }
                            break;
                        case PATCH_REMOVE:
                            try
                            {
                                RemoveFileDirectory(pi.dest_file, pi.patch_file);
                            }
                            catch
                            {
                                throw new Exception("Nelze smazat GRF soubor/adresar '" + pi.patch_file + "'");
                            }
                            break;
                    }
                    patchnum = pi.num;

                    try
                    {
                        stream = new FileStream("rebpatch.inf", FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.Write(BitConverter.GetBytes(patchnum), 0, 4);
                        stream.Close();
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Nelze zapsat aktuální číslo patche do souboru rebpatch.inf! ("+ Ex.Message + ").");
                    }
                }
                //pokial ma uzivatel custom patch subor:
                CustomPatche();
                setStatus("Reborn byl úspěšně opatchován. Můžeš spustit hru.");
                lblDownPrg.Text = "";
                EnableButton(btnStart, true);
                EnableButton(btnReplay, true);
                EnableButton(btnClose, true);
                EnableButton(btnSettings, true);

            }
            catch (WebException ex)
            {

                //if (ex.Response == null)
                //{
                //    setBrowserText(getHtmlHeader() + "<h3>Omlouváme se,</h3>Zřejmě nastala chyba na serveru Rebornu (nelze se připojit k serveru).<br><br>Prosím, počkej chvíli, poté zkus restartovat patcher. Pokud potíže nezmizí ani po několika pokusech tak nás kontaktuj.");
                //    setStatus("");
                //    return;
                //}

                //if (((HttpWebResponse)ex.Response).StatusCode != HttpStatusCode.OK)
                //{
                //    setBrowserText(getHtmlHeader() + "<h3>Omlouváme se,</h3>Zřejmě nastala chyba na serveru Rebornu (kód odpovědi není 200).<br><br>Prosím, počkej chvíli, poté zkus restartovat patcher. Pokud potíže nezmizí ani po několika pokusech tak nás kontaktuj.");
                //    setStatus("");
                //    return;
                //}

                if (ex.Response != null && ((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.ProxyAuthenticationRequired)
                {
                    LoginDialog log = new LoginDialog();
                    if (log.ShowDialog() == DialogResult.OK)
                    {
                        NetworkCredential cr = new NetworkCredential(log.login, log.password);
                        proxy.Credentials = cr;
                        PatchClient();
                    }
                    else
                    {
                        ProcessException(ex);
                    }
                }
                else ProcessException(ex);
                EnableButton(btnStart, true);
                EnableButton(btnReplay, true);
            }
            catch (ThreadAbortException)
            {
                setStatus("Patchovací proces byl přerušen.");
            }
            catch (Exception ex)
            {
                if (this.IsDisposed)
                {
                    ThrMain.Abort();
                    Application.Exit();
                }
                ProcessException(ex);
            }
            finally
            {
                if(ms != null)
                    ms.Close();

                if(reader != null)
                    reader.Close();

                if(stream != null)
                    stream.Close();

                if(xml != null)
                    xml.Close();
            }
        }

        /// <summary>
        /// Pridame uzivatelove custom patche
        /// </summary>
        public void CustomPatche()
        {
            if (File.Exists("custom.grf")) {
                string[] args = Environment.GetCommandLineArgs();
                int force = 0;
                
                foreach(string arg in args){
                    int result = String.Compare(arg, "-custom");
                    if (0 == result) {
                        force = 1;
                    }
                }
                if ((force == 1) || (File.GetCreationTime("custom.grf") > File.GetLastWriteTime("reborn.grf")) ||
                    (File.GetLastWriteTime("custom.grf") < File.GetLastWriteTime("reborn.grf"))) {

                    FileStream tmp_stream;
                    try
                    {
                        tmp_stream = new FileStream("reborn.grf", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                        tmp_stream.Close();
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    try
                    {
                        PatchFile("custom.grf", "reborn.grf");
                        File.SetLastWriteTime("custom.grf", DateTime.Now);
                    }
                    catch
                    {
                        throw new Exception("Nelze spojit GRF soubor 'custom.grf' do 'reborn.grf'");
                    }
                    if (force == 1) {
                        try
                            {
                                File.Delete("custom.grf");
                            }
                            catch
                            {
                                throw new Exception("Nelze smazat soubor 'custom.grf'. Restartuj počítač, a zkus to znova.");
                            }
                    }                        

                }
            }
        }

        /// <summary>
        /// Zpracovani vyjimky
        /// </summary>
        /// <param name="ex">vyjimka</param>
        [STAThread]
        public void ProcessException(Exception ex)
        {

            if (this.IsDisposed || this.Disposing)
            {
                ThrMain.Abort();
                Application.Exit();
                return;
            }
            EnableButton(btnClose, true);
            EnableButton(btnSettings, true);

            DoSchranky("Objevil se problém: [" + ex.Message + "] Stacktrace: [" + ex.StackTrace + "]");

            setBrowserText(getHtmlHeader() + @"
            <h3>Objevil se problém:</h3><font color='red'><b>" + ex.Message + "</b></font><br><br><u>Stacktrace:</u><br/>" + ex.StackTrace +
            "<br/><br/>Chyba byla zkopírována do schránky. Pokud problém přetrvává i po několika pokusech, nahlaš prosím tento problém na mail <a href='mailto:k3dt@reborn.cz'>k3dt@reborn.cz</a>.");

            setStatus("Objevil se problém. Nahlaš ho prosím na mail k3dt@reborn.cz");

            // zapisu jeste do logu...
            try
            {
                StreamWriter log = File.AppendText("Reborn.log");
                log.WriteLine(DateTime.Now+": Objevil se problém: [" + ex.Message + "] Stacktrace: [" + ex.StackTrace + "] Zdroj: [" + ex.Source + "]");
                log.Close();
            }
            catch { }
        }
        
        /// <summary>
        /// Funkce overujici rovnost bytovych poli
        /// </summary>
        /// <param name="ba1">pole 1</param>
        /// <param name="ba2">pole 2</param>
        /// <returns>true pokud jsou si pole rovna, false pokud nikoliv</returns>
        private bool ArraysEqual(byte[] ba1, byte[] ba2)
        {
            if (ba1 == null || ba2 == null)
                return false;

            if (ba1.Length != ba2.Length)
                return false;

            for (int i = 0; i < ba1.Length; i++)
            {
                if (ba1[i] != ba2[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Funkce na stahnuti souboru do patchdir adresare
        /// </summary>
        /// <param name="src">url souboru</param>
        /// <returns>0 pokud bylo stazeno, 1 pri chybe</returns>
        public int DownloadFile(String src, String dest)
        {
            BinaryReader reader = null;
            BinaryWriter writer = null;
            FileStream stream = null;
            int output = 0;
            try
            {
                //WebClient wc = new WebClient();
                try
                {
                    if (!Path.GetDirectoryName(dest).Equals("") && !Directory.Exists(Path.GetDirectoryName(dest))) Directory.CreateDirectory(Path.GetDirectoryName(dest));
                }
                catch (Exception Ex)
                {
                    throw new Exception("Nelze vytvořit adresář " + Path.GetDirectoryName(dest)+ " ("+Ex.Message+")");
                }
                setStatus("Stahování souboru '" + src + "'...");

                HttpWebRequest req;

                if (src.StartsWith("http")) {
                    req = (HttpWebRequest)WebRequest.Create(src);
                } else {
                    req = (HttpWebRequest)WebRequest.Create(root + src);
                }
                req.Credentials = CredentialCache.DefaultCredentials;

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                int size = (int)res.ContentLength;
                res.Close();

                reader = new BinaryReader(wc.OpenRead(src));
                stream = new FileStream(dest, FileMode.Create, FileAccess.Write, FileShare.None);
                writer = new BinaryWriter(stream);

                int read = 0;
                byte[] buffer = new byte[4096];
                int tick = Environment.TickCount, prev = 0, speed = 0, i = 0, diff = 0;

                while ((read = reader.Read(buffer, 0, buffer.Length)) > 0)
                {
                    writer.Write(buffer, 0, read);
                    diff = Environment.TickCount - tick;
                    if (diff >= 100)
                    {
                        i+=diff;
                        if (i >= 1000)
                        {
                            speed = ((int)writer.BaseStream.Length - prev) / diff;
                            i = 0;
                        }
                        tick = Environment.TickCount;
                        prev = (int)writer.BaseStream.Length;
                        if (speed > buffer.Length / 50) buffer = new byte[speed * 100];
                        UpdateDownloadProgress((int)writer.BaseStream.Length/1024, (int)size/1024, speed);
                    }
                }
                UpdateDownloadProgress(0,0,0);
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                output = 1;
            }
            finally
            {
                if(writer != null) writer.Close();
                if(stream != null) stream.Close();
                if(reader != null) reader.Close();
            }
            return output;

        }

        public int RemoveFileDirectory(String dest, String filename) {
            int output = 0;
            FileStream stream = null;
            BinaryWriter writer = null;
            MemoryStream ms = null;
            byte[] data = null;
            try
            {
                setStatus("Čtení hlaviček souborů...");
                GRFFileTable my_ft = GRF.GetFileTable(dest);
                if (my_ft.errcode > 0) throw new Exception(my_ft.errtext);
                
                int startoffset = (int)my_ft.offset;
                
                // soubory na svym puvodnim miste
                List<GRFFileInfo> non_changed = new List<GRFFileInfo>();
                // neaktualizovany soubory, ale presunuty na jiny misto v grf
                List<GRFFileInfo> non_changed_moved = new List<GRFFileInfo>();

                bool found = false;
                setStatus("Sestavování nové tabulky souborů...");
                int i = 0;
                int divider = my_ft.fileinfo.Length / 10;
                if (divider == 0) divider = 1;
                foreach (GRFFileInfo f_my in my_ft.fileinfo)
                {
                    if (System.Text.Encoding.GetEncoding("euc-kr").GetString(f_my.filename).IndexOf(filename) == -1)
                    {
                        if (f_my.offset < startoffset || f_my.flags != 1)
                        {
                            non_changed.Add(f_my); // soubory pod zacinajici offset pridat automaticky
                            found = true;
                        }
                        if (!found) non_changed_moved.Add(f_my); // nepatchovane soubory, ale presunute
                        else found = false;
                    }
                    i++;
                    if (i % divider == 0) UpdateProgress(i, my_ft.fileinfo.Length);
                }

                byte[] backup = new byte[my_ft.offset - startoffset];
                stream = new FileStream(dest, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                stream.Seek(46 + startoffset, SeekOrigin.Begin);
                int read = stream.Read(backup, 0, (int)(my_ft.offset - startoffset));

                if (read != (my_ft.offset - startoffset))
                    throw new Exception("Chyba! Cílový soubor " + dest + " je nejspíše poškozen.");

                writer = new BinaryWriter(stream);
                ms = new MemoryStream(backup);

                GRFFileTable newfiletable = new GRFFileTable();
                int index = 0, total = non_changed.Count + non_changed_moved.Count;
                newfiletable.fileinfo = new GRFFileInfo[total];
                foreach (GRFFileInfo fi in non_changed)
                {
                    newfiletable.fileinfo[index] = fi;
                    index++;
                    if (index % divider == 0) UpdateProgress(index, total);
                }
                writer.BaseStream.SetLength(46 + startoffset);
                writer.BaseStream.Seek(startoffset + 46, SeekOrigin.Begin);
                foreach (GRFFileInfo fi in non_changed_moved)
                {
                    data = new byte[fi.complen];
                    ms.Seek((int)fi.offset - startoffset, SeekOrigin.Begin);
                    read = ms.Read(data, 0, (int)fi.complen);

                    if (read != fi.complen)
                        throw new Exception("Chyba při zápisu souboru. GRF bude pravděpodobně poškozen.");

                    newfiletable.fileinfo[index] = fi;
                    newfiletable.fileinfo[index].offset = (UInt32)writer.BaseStream.Position - 46;
                    writer.Write(data);
                    index++;
                    if (index % divider == 0) UpdateProgress(index, total);
                }
                ms.Close();

                UpdateProgress(100, 100);
                newfiletable.offset = (UInt32)writer.BaseStream.Position - 46;
                writer.Write(GRF.MakeFileTable(newfiletable));
                writer.Seek(0, SeekOrigin.Begin);
                writer.Write(GRF.MakeFileHeader((int)newfiletable.offset, newfiletable.fileinfo.Length));
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                output = 1;
            }
            finally
            {
                if (stream != null) stream.Close();
                if (writer != null) writer.Close();
                if (ms != null) ms.Close();
            }
            return output;
        }

        /// <summary>
        /// Funkce spojujici 2 GRF soubory
        /// </summary>
        /// <param name="source">Soubor s patchem</param>
        /// <param name="dest">Soubor, do ktereho vkladame obsah patche</param>
        /// <returns>0 pokud uspesne probehlo patchovani, 1 pokud se vyskytla chyba</returns>
        public int PatchFile(String source, String dest)
        {
            int output = 0;
            FileStream stream = null;
            BinaryWriter writer = null;
            BinaryReader reader = null;
            MemoryStream ms = null;
            FileStream stream2 = null;
            byte[] data = null;
            try
            {
                setStatus("Čtení hlaviček souborů...");               
                GRFFileTable my_ft = GRF.GetFileTable(dest);
                if (my_ft.errcode > 0) throw new Exception(my_ft.errtext);
                GRFFileTable new_ft = GRF.GetFileTable(source);
                if (new_ft.errcode > 0) throw new Exception(new_ft.errtext);

                int startoffset = (int)my_ft.offset;

                setStatus("Hledání počátečního offsetu patche...");
                foreach (GRFFileInfo f_my in my_ft.fileinfo)
                {
                    if (f_my.offset > startoffset || f_my.flags != 1 ) continue;
                    foreach (GRFFileInfo f_new in new_ft.fileinfo)
                    {
                        if (f_my.flags == 1 && ArraysEqual(f_my.filename, f_new.filename))
                        {
                            if (f_my.offset < startoffset) startoffset = (int)f_my.offset;
                            break;
                        }
                    }
                }

                // soubory na svym puvodnim miste
                List<GRFFileInfo> non_changed = new List<GRFFileInfo>();
                // neaktualizovany soubory, ale presunuty na jiny misto v grf
                List<GRFFileInfo> non_changed_moved = new List<GRFFileInfo>(); 

                bool found = false;
                setStatus("Sestavování nové tabulky souborů...");
                int i = 0;
                int divider = my_ft.fileinfo.Length / 10;
                if(divider == 0) divider = 1;
                foreach (GRFFileInfo f_my in my_ft.fileinfo)
                {

                    if (f_my.offset < startoffset || f_my.flags != 1)
                    {
                        non_changed.Add(f_my); // soubory pod zacinajici offset pridat automaticky
                        found = true;
                    }
                    else foreach (GRFFileInfo f_new in new_ft.fileinfo)
                        {
                            if (ArraysEqual(f_my.filename, f_new.filename))
                            { // najdeme, jestli soubor nahodou neni aktualizovan
                                found = true;
                                break;
                            }
                        }
                    if (!found) non_changed_moved.Add(f_my); // nepatchovane soubory, ale presunute
                    else found = false;
                    i++;
                    if(i%divider == 0)UpdateProgress(i, my_ft.fileinfo.Length);
                }

                setStatus("Zálohování přepsaných dat...");
                byte[] backup = new byte[my_ft.offset - startoffset];
                stream = new FileStream(dest, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                stream.Seek(46+startoffset, SeekOrigin.Begin);
                int read = stream.Read(backup, 0, (int)(my_ft.offset - startoffset));
                
                if (read != (my_ft.offset - startoffset)) 
                    throw new Exception("Chyba! Cílový soubor " + dest + " je nejspíše poškozen.");
                
                stream2 = new FileStream(source, FileMode.Open, FileAccess.Read, FileShare.Read);
                
                writer = new BinaryWriter(stream);
                reader = new BinaryReader(stream2);
                ms = new MemoryStream(backup);

                setStatus("Zapisování dat souboru...");
                GRFFileTable newfiletable = new GRFFileTable();
                int index = 0, total = non_changed.Count + non_changed_moved.Count + new_ft.fileinfo.Length;
                newfiletable.fileinfo = new GRFFileInfo[total];
                foreach (GRFFileInfo fi in non_changed)
                {
                    newfiletable.fileinfo[index] = fi;
                    index++;
                    if (index % divider == 0) UpdateProgress(index, total);
                }
                writer.BaseStream.SetLength(46 + startoffset);
                writer.BaseStream.Seek(startoffset + 46, SeekOrigin.Begin);
                foreach (GRFFileInfo fi in non_changed_moved)
                {
                    data = new byte[fi.complen];
                    ms.Seek((int)fi.offset - startoffset, SeekOrigin.Begin);
                    read = ms.Read(data, 0, (int)fi.complen);
                    
                    if (read != fi.complen) 
                        throw new Exception("Chyba při zápisu souboru. GRF bude pravděpodobně poškozen.");
                    
                    newfiletable.fileinfo[index] = fi;
                    newfiletable.fileinfo[index].offset = (UInt32)writer.BaseStream.Position - 46;
                    writer.Write(data);
                    index++;
                    if (index % divider == 0) UpdateProgress(index, total);
                }
                ms.Close();

                foreach (GRFFileInfo fi in new_ft.fileinfo)
                {
                    data = new byte[fi.complen];
                    reader.BaseStream.Seek((int)fi.offset + 46, SeekOrigin.Begin);
                    read = reader.Read(data, 0, (int)fi.complen);
                    if (read != fi.complen) throw new Exception("Chyba při zápisu souboru. GRF bude pravděpodobně poškozen.");
                    newfiletable.fileinfo[index] = fi;
                    newfiletable.fileinfo[index].offset = (UInt32)writer.BaseStream.Position - 46;
                    writer.Write(data);
                    writer.Flush();
                    index++;
                    if (index % divider == 0) UpdateProgress(index, total);
                }
                UpdateProgress(100, 100);
                reader.Close();
                stream2.Close();
                newfiletable.offset = (UInt32)writer.BaseStream.Position - 46;
                writer.Write(GRF.MakeFileTable(newfiletable));
                writer.Seek(0, SeekOrigin.Begin);
                writer.Write(GRF.MakeFileHeader((int)newfiletable.offset, newfiletable.fileinfo.Length));
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                output = 1;
            }
            finally
            {
                if(stream != null)stream.Close();
                if(stream2 != null) stream2.Close();
                if(writer != null) writer.Close();
                if(reader != null) reader.Close();
                if(ms != null) ms.Close();
            }
            return output;

        }

        /// <summary>
        /// Funkce zapinajici vsechna tlacitka krome www
        /// </summary>
        public void EnableButtons()
        {
            EnableButton(btnClose, true);
            EnableButton(btnStart, true);
            EnableButton(btnSettings, true);
        }

        /// <summary>
        /// Funkce vypinajici vsechna tlacitka krome www
        /// </summary>
        public void DisableButtons()
        {
            EnableButton(btnClose, false);
            EnableButton(btnStart, false);
            EnableButton(btnSettings, false);
        }

        /// <summary>
        /// Funkce na vypnuti nejakehe tlacitka. V pripade potreby zavola obsluhujici thread
        /// </summary>
        /// <param name="button">tlacitko</param>
        /// <param name="enabled">ma-li byt tlacitko zapnuto, nebo vypnuto</param>
        private void EnableButton(Button button, bool enabled)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new EnableButtonCallback(EnableButton), button, enabled);
            }
            else
            {
                button.Enabled = enabled;
            }
        }

        /// <summary>
        /// Nastaveni progressbaru
        /// </summary>
        /// <param name="value">soucasna hodnota</param>
        /// <param name="maximum">maximalni hodnota</param>
        private void UpdateProgress(int value, int maximum)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateProgressCallback(UpdateProgress), value, maximum);
            }
            else
            {
                progBar.Maximum = maximum;
                progBar.Value = value;
            }
        }

        /// <summary>
        /// Nastaveni progressbaru a labelu. Pouziva se pri stahovani souboru.
        /// </summary>
        /// <param name="read">kolik uz bylo stazeno</param>
        /// <param name="total">jak je soubor veliky</param>
        /// <param name="speed">jakou rychlosti se stahuje</param>
        private void UpdateDownloadProgress(int read, int total, int speed)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new DownloadProgressCallback(UpdateDownloadProgress), read, total, speed);
            }
            else
            {
                if (speed == 0)
                    lblDownPrg.Text = String.Format("{0} / {1} kB", read, total);
                else
                    lblDownPrg.Text = String.Format("{0} / {1} kB ({2} kB/s)", read, total, speed);

                progBar.Maximum = (int)total;
                progBar.Value = (int)read;
            }
        }

        /// <summary>
        /// Nastaveni textu Labelu ve status baru
        /// </summary>
        /// <param name="lblStatus">toolStripLabel</param>
        /// <param name="p">text</param>
        private void setLabelText(ToolStripStatusLabel lblStatus, string p)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Nastavi text v prohlizeci
        /// </summary>
        /// <param name="text">text..</param>
        private void setBrowserText(String text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new BrowserCallback(setBrowserText), text);
            }
            else
            {
                if (browser.IsDisposed) return;
                browser.DocumentText = text;
                browser.Update();
            }
        }

        /// <summary>
        /// Nastaveni textu labelu
        /// </summary>
        /// <param name="label">label, kteremu nastavujeme text</param>
        /// <param name="text">text, ktery mu nastavujeme</param>
        private void setLabelText(Label label, String text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new LabelTextCallback(setLabelText), label, text);
            }
            else
            {
                label.Text = text;
            }
        }

        /// <summary>
        /// Nastavení barvy textu labeluu
        /// </summary>
        /// <param name="label">label</param>
        /// <param name="color">barva</param>
        private void setLabelColor(Label label, Color color)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new LabelColorCallback(setLabelColor), label, color);
            }
            else
            {
                label.ForeColor = color;
            }
        }

        /// <summary>
        /// Nastaveni statusu (do labelu ve status baru
        /// </summary>
        /// <param name="text">status text</param>
        public void setStatus(String text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new StatusTextCallback(setStatus), text);
            }
            else
            {
                lblStatus.Text = text;
            }

        }

        /// <summary>
        /// Ukonceni patcheru
        /// </summary>
        private void brnClose_Click(object sender, EventArgs e)
        {
            if (ThrMain != null && ThrMain.IsAlive)
                ThrMain.Abort();

            Application.Exit();
        }

        /// <summary>
        /// spusteni hry a ukonceni patcheru
        /// </summary>
        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                btnStart.Enabled = false;
                Process.Start("reexe.exe");
                Application.Exit();
            }
            catch (Exception ex)
            {
                btnStart.Enabled = true;
                browser.DocumentText = ex.Message;
            }
        }

        /// <summary>
        /// Zruseni threadu pokud se ukoncuje..
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ThrMain != null) ThrMain.Abort();
        }

        /// <summary>
        /// Vyovolani okna nastaveni
        /// </summary>
        private void btnSettings_Click(object sender, EventArgs e)
        {
            if (setForm == null)
            {
                setForm = new formSettings(this, settings);
                setForm.StartPosition = FormStartPosition.Manual;
                setForm.SetDesktopLocation(this.Location.X + this.Size.Width, this.Location.Y);
                setForm.FormClosed += delegate { setForm = null; };

                if (!setForm.Visible) setForm.Visible = true;
                if (setForm.WindowState == FormWindowState.Minimized)
                    setForm.WindowState = FormWindowState.Normal;
                setForm.BringToFront();
            }
            else
            {
                setForm.Close();
            }            
        }

        /// <summary>
        /// Nacteni nastaveni
        /// </summary>
        public void LoadSettings()
        {
            BinaryReader reader = null;
            int i = 0;
            try
            {
                reader = new BinaryReader(new FileStream("rebpatch.inf", FileMode.Open, FileAccess.Read, FileShare.Read));
                reader.BaseStream.Seek(4, SeekOrigin.Begin);
                for (i = 0; i < SETTINGS_COUNT; i++)
                {
                    try
                    {
                        settings[i] = reader.ReadByte();
                    }
                    catch (Exception)
                    {
                        settings[i] = 0;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Vrati HTML header textu.
        /// </summary>
        /// <returns>String reprezentace HTML kodu az po body</returns>
        private String getHtmlHeader()
        {
            return @"<HTML>
                <HEAD>
                  <TITLE>
                    Reborn Patcher
                  </TITLE>

                  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></meta>

                  <STYLE>
                    BODY
                    {
                      scrollbar-arrow-color: #C0C0C0;
                      scrollbar-track-color: #FFFFFF;
                      scrollbar-face-color: #FFFFFF;
                      scrollbar-highlight-color: #C0C0C0;
                      scrollbar-3dlight-color: #FFFFFF;
                      scrollbar-darkshadow-color: #FFFFFF;
                      scrollbar-shadow-color: #C0C0C0;
                      background-color:#FFFFFF;
                    }
                      A:link { 
                        color: black;
                      }
                      
                      A:visited {
                        color: black;
                      }

                      A:active {
                        color: black;
                      }

                      A:hover {
                        color: #34498B;
                      }
                      
                      body {
                        font-size: 11px;
                        FONT-FAMILY: Tahoma;
                      }
                  </STYLE>
                </HEAD>

                <BODY>
                ";
        }

        private int typeToInt(String type)
        {
            switch (type)
            {
                case "rewrite":
                    return PATCH_REWRITE;
                case "delete":
                    return PATCH_DELETE;
                case "patch":
                    return PATCH_PATCH;
                case "text":
                    return PATCH_TEXT;
                case "remove":
                    return PATCH_REMOVE;
                default:
                    return PATCH_INVALID;
            }
        }

        private String typeToStr(int type)
        {
            switch (type)
            {
                case PATCH_REWRITE:
                    return "rewrite";
                case PATCH_DELETE:
                    return "delete";
                case PATCH_PATCH:
                    return "patch";
                case PATCH_TEXT:
                    return "text";
                case PATCH_REMOVE:
                    return "remove";
                default:
                    return "invalid";
            }

        }

        /// <summary>
        /// Patchne EXE pro vypnuti/zapnuti shake efektu
        /// </summary>
        /// <param name="enable">Zapnout antishake?</param>
        public bool setAntishake(bool enable)
        {
            try
            {
                byte[] search;
                byte[] replace;

                if (enable)
                {
                    search = new byte[] { 0xD9,0x44,0x24,0x04,0xD9,0x59,0x04,0xD9,0x44,0x24,0x0C,0xD9,0x59,0x0C,0xD9,0x44,0x24,0x08,0xD9,0x59,0x08,0xC2,0x0C,0x00,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0x8B,0x44,0x24,0x04 };
                    replace = new byte[] {0xC2,0x0C,0x00,0x90,0xD9,0x59,0x04,0xD9,0x44,0x24,0x0C,0xD9,0x59,0x0C,0xD9,0x44,0x24,0x08,0xD9,0x59,0x08,0xC2,0x0C,0x00,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xC2,0x14,0x00,0x90 };

                }
                else
                {
                    replace = new byte[] { 0xD9, 0x44, 0x24, 0x04, 0xD9, 0x59, 0x04, 0xD9, 0x44, 0x24, 0x0C, 0xD9, 0x59, 0x0C, 0xD9, 0x44, 0x24, 0x08, 0xD9, 0x59, 0x08, 0xC2, 0x0C, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0x8B, 0x44, 0x24, 0x04 };
                    search = new byte[] { 0xC2, 0x0C, 0x00, 0x90, 0xD9, 0x59, 0x04, 0xD9, 0x44, 0x24, 0x0C, 0xD9, 0x59, 0x0C, 0xD9, 0x44, 0x24, 0x08, 0xD9, 0x59, 0x08, 0xC2, 0x0C, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xC2, 0x14, 0x00, 0x90 };
                }

                FileStream fs = new FileStream("reexe.exe", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                BufferedStream bs = new BufferedStream(fs);
                byte[] buffer = new byte[bs.Length];

                int f = 0, j = 0;
                int read;
                bool found = false;

                read = bs.Read(buffer, 0, buffer.Length);
                for (int i = 0; i < read; i++)
                {
                    if (buffer[i] == search[f]) f++;
                    else f = 0;

                    if (!found && f == search.Length)
                    {
                        found = true; f = 0;
                        j = i - search.Length + 1;
                        bs.Seek(j, SeekOrigin.Begin);
                        bs.Write(replace, 0, replace.Length);
                    }
                }

                bs.Close();
                return found;

            }
            catch (Exception ex)
            {
                ProcessException(ex);
                return false;
            }
        }
        
        /// <summary>
        /// Upravi volani registru v souboru na HKLM nebo HKCU, podle zadanych bytu
        /// </summary>
        /// <param name="filename">Jmeno binarniho souboru, ktery se bude upravovat</param>
        /// <param name="searchbyte">Byte, ktery hledame.. 0x01 = HKCU, 0x02 = HKLM</param>
        /// <param name="replacebyte">Byte, kterym hledany nahradime.. 0x01 = HKCU, 0x02 = HKLM</param>
        public bool registryPatch(String filename, byte searchbyte, byte replacebyte)
        {
            try
            {
                FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                BufferedStream bs = new BufferedStream(fs);
                byte[] buffer = new byte[bs.Length];
                int read, f = 0, found = 0;
                byte[] search = { 0x68, searchbyte, 0x00, 0x00, 0x80 };

                read = bs.Read(buffer, 0, buffer.Length);
                for (int i = 0; i < read; i++)
                {
                    if (buffer[i] == search[f]) f++;
                    else f = 0;
                    if (f == 5)
                    {
                        f = 0;
                        found++;
                        bs.Seek(i - 3, SeekOrigin.Begin);
                        bs.WriteByte(replacebyte);
                    }
                }
                bs.Close();
                return found>0?true:false;
            }
            catch (Exception ex)
            {
                ProcessException(ex);
                return false;
            }

        }

        private void btnStart_Paint(object sender, PaintEventArgs e)
        {
        }

        private void formPatcher_Shown(object sender, EventArgs e)
        {

            try
            {
                String[] args = Environment.GetCommandLineArgs();
                lblStatus.Text = "";

                try
                {
                    Directory.SetCurrentDirectory(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
                }
                catch (Exception Ex)
                {
                    throw new Exception("Nelze nastavit cestu k aktuálnímu adresáři! (" + Ex.Message + ").");
                }

                if (File.Exists("Updater.exe"))
                    try
                    {
                        File.Delete("Updater.exe");
                    }

                    catch (Exception) { };

                if (!File.Exists("data.grf") && !File.Exists("sdata.grf"))
                {
                    throw new Exception("Chybí ti soubor data.grf/sdata.grf. Patcher se zřejmě nenachází v adresáři s hrou. Zkopíruj ho do složky kde máš nainstalovaný Ragnarok.");
                }

                if (!File.Exists("reexe.exe"))
                {
                    throw new Exception("Chybí ti soubor reexe.exe. Patcher se zřejmě nenachází v adresáři s hrou. Zkopíruj ho do složky kde máš nainstalovaný Ragnarok.");
                }

                if (!File.Exists("setup.exe"))
                {
                    throw new Exception("Chybí ti soubor setup.exe. Patcher se zřejmě nenachází v adresáři s hrou. Zkopíruj ho do složky kde máš nainstalovaný Ragnarok.");
                }

                if (!Directory.Exists("data/"))
                {
                    setStatus("Vytvářím složku data/...");
                    try
                    {
                        Directory.CreateDirectory("data/");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Nelze vytvořit adresář data/ v adresáři hry! ("+Ex.Message+").");
                    }

                }

                // zapnem thread kterej zjisti jestli existujou novy patche, pripadne opatchuje
                try
                {
                    ThrMain = new Thread(new ThreadStart(PatchClient));
                    ThrMain.Start();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Objevil se problém s patchovacím threadem ("+Ex.Message+").");
                }
            }
            catch (Exception Ex)
            {
                ProcessException(Ex);
            }

        }

        private void formPatcher_Load(object sender, EventArgs e)
        {
            browser.ObjectForScripting = this;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                btnStart.Enabled = false;
                Process.Start("reexe.exe","-Replay");
                Application.Exit();
            }
            catch (Exception ex)
            {
                btnStart.Enabled = true;
                browser.DocumentText = ex.Message;
            }
        }

    }
}
