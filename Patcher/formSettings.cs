﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Threading;
using Microsoft.Win32;
using System.Diagnostics;
using System.Net.Sockets;

namespace Patcher
{
    public partial class formSettings : Form
    {
        formPatcher fp;
        private int SETTINGS_COUNT;
        byte[] settings;

        private delegate void LabelTextCallback(Label label, String text);
        private delegate void LabelColorCallback(Label label, Color color);

        public const int
            SET_EFF = 0,
            SET_PETS = 1,
            SET_MF = 2,
            SET_HKCU = 3,
            SET_ANTISHAKE = 4,
            SET_SCREEN = 5;

        public const int
            SET_EFF_STANDARD = 0,
            SET_EFF_MINIMAL = 1,
            SET_EFF_GW = 2,

            SET_PETS_CZECH = 0,
            SET_PETS_OFF = 1,

            SET_MF_EXT = 0,
            SET_MF_EXE = 1,
            SET_MF_DLL = 2,

            SET_HKCU_HKLM = 0,
            SET_HKCU_HKCU = 1,

            SET_SCREEN_WINDOW = 0,
            SET_SCREEN_FULLSCREEN = 1,

            SET_ANTISHAKE_OFF = 0,
            SET_ANTISHAKE_ON = 1;

        private Thread ThrLogin, ThrChar, ThrMap, ThrWeb;
        private bool portLogin = false, portChar = false, portMap = false, portWeb = false;

        /// <summary>
        /// Nastaveni textu labelu
        /// </summary>
        /// <param name="label">label, kteremu nastavujeme text</param>
        /// <param name="text">text, ktery mu nastavujeme</param>
        private void setLabelText(Label label, String text)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new LabelTextCallback(setLabelText), label, text);
            }
            else
            {
                label.Text = text;
            }
        }

        /// <summary>
        /// Nastavení barvy textu labeluu
        /// </summary>
        /// <param name="label">label</param>
        /// <param name="color">barva</param>
        private void setLabelColor(Label label, Color color)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new LabelColorCallback(setLabelColor), label, color);
            }
            else
            {
                label.ForeColor = color;
            }
        }


        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            try
            {
                if (msg.WParam.ToInt32() == (int)Keys.Escape)
                {
                    this.Close();
                    fp.BringToFront();
                }
                else
                {
                    return base.ProcessCmdKey(ref msg, keyData);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Chyba při mapovaní klávesy ESC: " + Ex.Message);
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// Konstruktor tridy
        /// </summary>
        /// <param name="fp">formPatcher - abychom k nemu mohli pristupovat</param>
        /// <param name="settings">soucasna nastaveni</param>
        public formSettings(formPatcher fp, byte[] settings)
        {
            this.fp = fp;
            this.settings = settings;
            SETTINGS_COUNT = settings.Length;
            InitializeComponent();
        }

        /// <summary>
        /// Zruseni nastaveni bez aplikovani zmen
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            fp.BringToFront();
        }

        private void rbGrEfStandard_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Standardní sada efektů." +
                "Všechno je podle původních korejských souborů.";
        }

        private void rbGrEfOsek_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Osekané efekty plošných kouzel a Asury." +
                "Hodí se, pokud potřebuješ lepší přehled.";
        }

        private void rbPetsOff_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Vypnuty všechny hlášky petů.";
        }

        private void rbPetsCzech_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Peti na tebe ve hře budou mluvit česky :)";
        }

        private void rbMFExe_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Použije soubor RO_MF.exe, které se musí spustit dříve, než se spustí hra." +
                "Funguje spolehlivě na všech verzích Windows.";
        }

        private void rbMFDll_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Použije soubor dinput.dll, který si hra načítá automaticky při spuštění.";
        }
        /*
        private void rbExeStandard_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Standardní spouštěcí soubor takový, jaký ho znáš. Nedisponuje ničím navíc.";
        }

        private void rbExeHKCU_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Pokud nemáš práva pro zápis do registrů a hra se ti nechce spustit," +
                "použijš toto exe. Je stejné jako standardní, pouze cesta pro ukládání nastavení" +
                "je v uživatelské oblasti. Budeš muset spustit Setup.exe.";
        }
        */

        private void rbScreenFullScreen_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Spustí hru na celú obrazovku.";
        }

        private void rbScreenWindow_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Spustí hru v okne.";
        }

        /// <summary>
        /// Zobrazeni vsech nastaveni podle toho, jak jsme to dostali od hlavniho formu
        /// </summary>
        private void formSettings_Shown(object sender, EventArgs e)
        {
            for (int i = 0; i < SETTINGS_COUNT; i++)
            {
                switch (i)
                {
                    case SET_EFF: // efekty
                        switch (settings[i])
                        {
                            case SET_EFF_STANDARD: rbGrEfStandard.Checked = true; break;
                            case SET_EFF_MINIMAL: rbGrEfOsek.Checked = true; break;
                            case SET_EFF_GW: rbGrEfGW.Checked = true; break;
                        }
                        break;
                    case SET_PETS: // peti
                        switch (settings[i])
                        {
                            case SET_PETS_CZECH: rbPetsCzech.Checked = true; break;
                            case SET_PETS_OFF: rbPetsOff.Checked = true; break;
                        }
                        break;
                    /*case SET_MF: // Mouse Freedom
                        switch (settings[i])
                        {
                            case SET_MF_EXT: rbMFExt.Checked = true; break;
                            case SET_MF_EXE: rbMFExe.Checked = true; break;
                            case SET_MF_DLL: rbMFDll.Checked = true; break;
                        }
                        break;*/
                    /*case SET_HKCU: // hklm ? hkcu
                        switch (settings[i])
                        {
                            case SET_HKCU_HKLM: rbExeStandard.Checked = true; break;
                            case SET_HKCU_HKCU: rbExeHKCU.Checked = true; break;
                        }
                        break;*/
                    case SET_SCREEN: // windowed/FS? opensetup ho neuklada:/
                        switch (settings[i])
                        {
                            case SET_SCREEN_WINDOW: rbScreenWindow.Checked = true; break;
                            case SET_SCREEN_FULLSCREEN: rbScreenFullScreen.Checked = true; break;
                        }
                        break;
                    case SET_ANTISHAKE:
                        switch (settings[i])
                        {
                            case SET_ANTISHAKE_OFF: antishake.Checked = false; break;
                            case SET_ANTISHAKE_ON: antishake.Checked = true; break;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Po stisku tlacitka Aplikovat zmeny se spusti novy thread, ktery je aplikuje
        /// </summary>
        private void btnApply_Click(object sender, EventArgs e)
        {
            Thread thr = new Thread(new ThreadStart(ApplySettings));
            thr.Start();
            this.Close();


        }

        /// <summary>
        /// Funkce, ktera prohlizi, jestli se neco zmenilo a pokud ano, zavola funkci na provedeni zmeny
        /// </summary>
        private void ApplySettings()
        {

            FileStream tmp_stream;
            try
            {
                tmp_stream = new FileStream("reborn.grf", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                tmp_stream.Close();
                tmp_stream = new FileStream("palety.grf", FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                tmp_stream.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Pozor! Máš spuštěný Ragnarok! Pokud chceš změnit nastavení, musíš nejdřív vypnout hru a znovu pustit patcher.\n(Nelze otevřít soubory reborn.grf a palety.grf pro zápis)", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            BinaryWriter wr = null;
            try
            {
                fp.DisableButtons();
                byte[] current = new byte[SETTINGS_COUNT];
                if (rbGrEfStandard.Checked) current[SET_EFF] = SET_EFF_STANDARD;
                else if (rbGrEfOsek.Checked) current[SET_EFF] = SET_EFF_MINIMAL;
                else if (rbGrEfGW.Checked) current[SET_EFF] = SET_EFF_GW;

                if (rbPetsCzech.Checked) current[SET_PETS] = SET_PETS_CZECH;
                else if (rbPetsOff.Checked) current[SET_PETS] = SET_PETS_OFF;

                /*if (rbMFExt.Checked) current[SET_MF] = SET_MF_EXT;
                else if (rbMFExe.Checked) current[SET_MF] = SET_MF_EXE;
                else if (rbMFDll.Checked) current[SET_MF] = SET_MF_DLL;
                */
                if (rbScreenWindow.Checked) current[SET_SCREEN] = SET_SCREEN_WINDOW;
                else if (rbScreenFullScreen.Checked) current[SET_SCREEN] = SET_SCREEN_FULLSCREEN;

                if (antishake.Checked) current[SET_ANTISHAKE] = SET_ANTISHAKE_ON;
                else current[SET_ANTISHAKE] = SET_ANTISHAKE_OFF;

                wr = new BinaryWriter(new FileStream("rebpatch.inf", FileMode.OpenOrCreate, FileAccess.Write, FileShare.None));
                for (int i = 0; i < SETTINGS_COUNT; i++)
                {
                    if (current[i] != settings[i])
                    {
                        if (ApplyPatch(i, current[i]))
                        {
                            wr.BaseStream.Seek(4 + i, SeekOrigin.Begin);
                            wr.Write(current[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fp.ProcessException(ex);
            }
            finally
            {
                if (wr != null) wr.Close();
                fp.LoadSettings();
                fp.EnableButtons();
            }
        }

        /// <summary>
        /// Provedeni zmen
        /// </summary>
        /// <param name="num">cislo nastaveni</param>
        /// <param name="opt">hodnota</param>
        /// <returns>true pokud byla zmena provedena, false pokud nastala chyba</returns>
        private bool ApplyPatch(int num, int opt)
        {
            bool output = false;
            String statusmes = "";
            string[] delfiles = null;
            string[] newfiles = null;
            string[] new_remote = null;
            switch (num)
            {
                case SET_EFF: // efekty
                    switch (opt)
                    {
                        case SET_EFF_STANDARD: // standard
                            newfiles = new string[1] { "nastaveni/_rebEffStandard.grf" };
                            statusmes = "Nahrány standardní efekty.";
                            break;
                        case SET_EFF_MINIMAL: // osekane
                            newfiles = new string[1] { "nastaveni/_rebEffOsek.grf" };
                            statusmes = "Nahrány osekané efekty.";
                            break;
                        case SET_EFF_GW: //gw
                            newfiles = new string[1] { "nastaveni/_rebEffGW.grf" };
                            statusmes = "Nahrány GW efekty.";
                            break;
                    }
                    if (newfiles == null) break;

                    if (!File.Exists(fp.patchdir + newfiles[0]))
                        if (fp.DownloadFile(newfiles[0], fp.patchdir + newfiles[0]) > 0)
                            throw new Exception("Nepodařilo se stáhnout soubor s patchem pro nastavení efektů.");

                    fp.PatchFile(fp.patchdir + newfiles[0], "reborn.grf");

                    fp.setStatus(statusmes);
                    output = true;
                    break;


                case SET_PETS: // peti
                    switch (opt)
                    {
                        case SET_PETS_CZECH: // standard
                            newfiles = new string[1] { "nastaveni/_rebPetsOn.grf" };
                            statusmes = "Hlášky petů zapnuty.";
                            break;
                        case SET_PETS_OFF: // vyp
                            newfiles = new string[1] { "nastaveni/_rebPetsOff.grf" };
                            statusmes = "Hlášky petů vypnuty.";
                            break;
                    }
                    if (newfiles == null) break;

                    if (!File.Exists(fp.patchdir + newfiles[0]))
                        if (fp.DownloadFile(newfiles[0], fp.patchdir + newfiles[0]) > 0)
                            throw new Exception("Nepodařilo se stáhnout soubor s patchem pro nastavení petů.");

                    fp.PatchFile(fp.patchdir + newfiles[0], "reborn.grf");

                    fp.setStatus(statusmes);
                    output = true;
                    break;


                case SET_MF: // mouse freedom
                    switch (opt)
                    {
                        case SET_MF_EXT: // ext
                            delfiles = new string[3] { "RO_MF.bin", "RO_MF.dll", "RO_MF.exe" };
                            newfiles = new string[2] { "dinput.dll", "dinput.ini" };
                            new_remote = new string[2] { "nastaveni/dinput_ext.dll", "nastaveni/dinput_ext.ini" };
                            statusmes = "Bylo aktivováno ROExt.";
                            break;
                        case SET_MF_EXE: // exe
                            delfiles = new string[1] { "dinput.dll" };
                            newfiles = new string[4] { "RO_MF.bin", "RO_MF.dll", "RO_MF.exe", "RO_MF.ini" };
                            new_remote = new string[4] { "nastaveni/RO_MF.bin", "nastaveni/RO_MF.dll", "nastaveni/RO_MF.exe", "nastaveni/RO_MF.ini" };
                            statusmes = "Byla aktivována EXE verze RO Mouse Freedom.";
                            break;
                        case SET_MF_DLL: // dll_old
                            delfiles = new string[3] { "RO_MF.bin", "RO_MF.dll", "RO_MF.exe" };
                            newfiles = new string[3] { "dinput.dll", "RO_MF.ini", "RO_MF.bin" };
                            new_remote = new string[3] { "nastaveni/dinput_old.dll", "nastaveni/RO_MF_old.ini", "nastaveni/RO_MF_old.bin" };
                            statusmes = "Byla aktivována DLL verze RO Mouse Freedom.";
                            break;


                    }

                    //killnem pripadny bezici ro_mf pokud je mezi souborama ke smazani
                    if (Array.IndexOf(delfiles, "RO_MF.exe") >= 0)
                    {

                        foreach (Process pr in Process.GetProcesses())
                        {
                            if (pr.ProcessName.Equals("RO_MF") && Path.GetDirectoryName(pr.MainModule.FileName).Equals(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName)))
                            {
                                pr.Kill();
                                System.Threading.Thread.Sleep(500);
                            }
                        }
                    }

                    foreach (string del in delfiles)
                    {
                        if (File.Exists(del)) File.Delete(del);
                    }
                    for (int i = 0; i < newfiles.Length; i++)
                    {
                        if (!File.Exists(fp.patchdir + new_remote[i]))
                            if (fp.DownloadFile(new_remote[i], fp.patchdir + new_remote[i]) > 0)
                                throw new Exception("Nepodařilo se stáhnout soubor '" + new_remote[i] + "'.");

                        if (!File.Exists(newfiles[i])) File.Copy(fp.patchdir + new_remote[i], newfiles[i]);
                    }
                    fp.setStatus(statusmes);
                    output = true;
                    break;
                case SET_HKCU: // hklm ? hkcu
                    switch (opt)
                    {
                        case SET_HKCU_HKLM:
                            if (fp.registryPatch("reexe.exe", 0x01, 0x02) && fp.registryPatch("Setup.exe", 0x01, 0x02))
                            {
                                fp.setStatus("Soubory setup.exe a reexe.exe byly opatchovány na HKLM verzi.");
                                output = true;
                            }
                            break;
                        case SET_HKCU_HKCU:
                            if (fp.registryPatch("reexe.exe", 0x02, 0x01) && fp.registryPatch("Setup.exe", 0x02, 0x01))
                            {
                                fp.setStatus("Soubory setup.exe a reexe.exe byly opatchovány na HKCU verzi.");
                                output = true;
                            }
                            break;
                    }
                    break;
                case SET_SCREEN:
                    switch (opt)
                    {
                        case SET_SCREEN_WINDOW:
                            try
                            {
                                Registry.CurrentUser.CreateSubKey("Software\\Gravity Soft\\Ragnarok\\");
                                Registry.CurrentUser.OpenSubKey("Software\\Gravity Soft\\Ragnarok",true).SetValue("ISFULLSCREENMODE", 0x0, RegistryValueKind.DWord); ;
                                output = true;
                            }
                            catch (ArgumentException)
                            {
                                fp.setStatus("V sekci registrů HKLM nebylo nalezeno žádné nastavení Ragnaroku.");
                            }
                            break;
                        case SET_SCREEN_FULLSCREEN:
                            try
                            {
                                Registry.CurrentUser.CreateSubKey("Software\\Gravity Soft\\Ragnarok\\");
                                Registry.CurrentUser.OpenSubKey("Software\\Gravity Soft\\Ragnarok",true).SetValue("ISFULLSCREENMODE", 0x1, RegistryValueKind.DWord); ;
                                output = true;
                            }
                            catch (ArgumentException)
                            {
                                fp.setStatus("V sekci registrů HKLM nebylo nalezeno žádné nastavení Ragnaroku.");
                            }
                            break;
                    }
                    break;
                case SET_ANTISHAKE:
                    switch (opt)
                    {
                        case SET_ANTISHAKE_OFF:
                            if (fp.setAntishake(false))
                            {
                                fp.setStatus("Antishake byl úspěšně vypnut.");
                                output = true;
                            }
                            else fp.setStatus("Seek pattern nebyl nalezen. Zkus reset patchů.");
                            break;
                        case SET_ANTISHAKE_ON:
                            if (fp.setAntishake(true))
                            {
                                fp.setStatus("Antishake byl úspěšně zapnut.");
                                output = true;
                            }
                            else fp.setStatus("Seek pattern nebyl nalezen. Zkus reset patchů.");
                            break;
                    }
                    break;
            }
            return output;
        }

        private void btnCancel_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Neprovede žádné změny a zavře okno.";
        }

        private void btnApply_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Provede změny a uloží nastavení.";
        }

        private void rbMFExt_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Pokročilejší verze Mouse Freedom." +
                "Možnost nastavení v souboru dinput.ini.";
        }

        private void btnRegReset_MouseEnter(object sender, EventArgs e)
        {
            tbInfo2.Text = "Smaže nastavení Ragnaroku ve tvém počítači.";
        }

        private void btnPatchReset_MouseEnter(object sender, EventArgs e)
        {
            tbInfo2.Text = "Znovu stáhne všechny patche od začátku.";
        }

        private void btnSdataReset_MouseEnter(object sender, EventArgs e)
        {
            tbInfo2.Text = "Znovu stáhne soubor data.grf (jen v krajni nouzi!).";
        }

        private void btnRegReset_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Opravdu chceš smazat nastavení Ragnaroku?", "Smazání nastavení", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            int count = 0;
            try
            {
                Registry.CurrentUser.DeleteSubKeyTree(@"Software\Classes\VirtualStore\MACHINE\SOFTWARE\Gravity Soft\Ragnarok");
                count++;
            }
            catch (ArgumentException)
            {
            }

            try
            {
                Registry.CurrentUser.DeleteSubKeyTree(@"Software\Gravity Soft\Ragnarok");
                count++;
            }
            catch (ArgumentException)
            {
            }

            try
            {
                Registry.LocalMachine.DeleteSubKeyTree(@"SOFTWARE\Gravity Soft\Ragnarok");
                count++;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("V sekci registrů HKLM nebylo nalezeno žádné nastavení Ragnaroku.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            string hlaska;
            switch (count)
            {
                case 0:
                    hlaska = "Nesmazány žádné klíče.";
                    break;
                case 1:
                    hlaska = "Smazán jeden klíč s nastavením RO.";
                    break;
                default:
                    hlaska = "Smazáno " + count + " klíčů s nastavením RO.";
                    break;
            }
            MessageBox.Show(hlaska, "Smazání registrů", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Process.Start("Setup.exe");
        }

        private void btnPatchReset_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Opravdu chceš stáhnout všechny patche od začátku? Může to chvilku trvat.", "Upozornění", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    BinaryWriter br = new BinaryWriter(new FileStream("rebpatch.inf", FileMode.Open, FileAccess.Write, FileShare.None));
                    // aktualni patch = 0
                    br.Write((UInt32)0);

                    // nastaveni efektu = 0
                    br.Seek(4, SeekOrigin.Begin);
                    br.Write(0);

                    // nastaveni petu = 0
                    br.Seek(5, SeekOrigin.Begin);
                    br.Write(0);

                    br.Close();

                    settings[SET_EFF] = SET_EFF_STANDARD;
                    rbScreenWindow.Checked = true;

                    if (File.Exists("custom.grf")) {
                        if (File.Exists("custom.BAK")) {
                            File.Delete("custom.BAK");
                        }
                        File.Move("custom.grf", "custom.BAK");
                    }

                    fp.ThrMain = new Thread(new ThreadStart(fp.PatchClient));
                    fp.ThrMain.Start();
                    this.Close();
                    fp.BringToFront();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("Chyba při resetu patchů: "+Ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSdataReset_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Opravdu chceš stáhnout soubor data.grf (1,76 GB) od začátku? Může to chvilku trvat.", "Upozornění", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            try {
                fp.setStatus("Stahuji data.grf. Bude to chvíli trvat...");
                if (fp.DownloadFile("http://b.itech.sk/public/ROdata/data.grf", "data.grf.new") > 0)
                    throw new Exception("Chyba při stahování souboru 'data.grf'");

                if (!File.Exists("data.grf.new"))
                    throw new Exception("Nenalezen soubor 'data.grf.new' - asi se nestáhl správně.");

                if (File.Exists("data.grf")) {
                    try
                    {
                        if (File.Exists("data.grf.old")) {
                             File.Delete("data.grf.old");
                        }
                        File.Move("data.grf", "data.grf.old");
                    }
                    catch
                    {
                        throw new Exception("Nelze smazat soubor 'data.grf'. Restartuj počítač, a zkus to znova.");
                    }
                }
                try
                {
                    File.Move("data.grf.new", "data.grf");
                }
                catch
                {
                    throw new Exception("Nelze zkopírovat soubor 'data.grf.new' na soubor 'data.grf'. Restartuj počítač, a zkus to znova.");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Chyba při resetu data.grf: " + Ex.Message);
            }
        }

        private void btnPortTest_Click(object sender, EventArgs e)
        {
            if (ThrLogin == null || (!portLogin && !ThrLogin.IsAlive))
            {
                ThrLogin = new Thread(new ThreadStart(LoginPortTest));
                ThrLogin.Start();
            }
            if (ThrChar == null || (!portChar && !ThrChar.IsAlive))
            {
                ThrChar = new Thread(new ThreadStart(CharPortTest));
                ThrChar.Start();
            }
            if (ThrMap == null || (!portMap && !ThrMap.IsAlive))
            {
                ThrMap = new Thread(new ThreadStart(MapPortTest));
                ThrMap.Start();
            }
            if (ThrWeb == null || (!portWeb && !ThrWeb.IsAlive))
            {
                ThrWeb = new Thread(new ThreadStart(WebPortTest));
                ThrWeb.Start();
            }
        }

        private void LoginPortTest()
        {
            setLabelText(lblLogin, "...");
            try
            {
                TcpClient tcp = new TcpClient();
                tcp.Connect("server.reborn.cz", 6900);
                setLabelColor(lblLogin, System.Drawing.Color.Green);
                setLabelText(lblLogin, "V pořádku");
                tcp.Close();
                portLogin = true;
            }
            catch (Exception)
            {
                setLabelColor(lblLogin, System.Drawing.Color.Red);
                setLabelText(lblLogin, "Chyba");
            }

        }

        private void CharPortTest()
        {
            setLabelText(lblChar, "...");
            try
            {
                TcpClient tcp = new TcpClient();
                tcp.Connect("server.reborn.cz", 7000);
                setLabelColor(lblChar, System.Drawing.Color.Green);
                setLabelText(lblChar, "V pořádku");
                tcp.Close();
                portChar = true;
            }
            catch (Exception)
            {
                setLabelColor(lblChar, System.Drawing.Color.Red);
                setLabelText(lblChar, "Chyba");
            }

        }

        private void MapPortTest()
        {
            setLabelText(lblMap, "...");
            try
            {
                TcpClient tcp = new TcpClient();
                setLabelColor(lblMap, System.Drawing.Color.Green);
                setLabelText(lblMap, "V pořádku");
                tcp.Close();
                portMap = true;
            }
            catch (Exception)
            {
                setLabelColor(lblMap, System.Drawing.Color.Red);
                setLabelText(lblMap, "Chyba");
            }

        }

        private void WebPortTest()
        {
            setLabelText(lblWeb, "...");
            try
            {
                TcpClient tcp = new TcpClient();
                tcp.Connect("server.reborn.cz", 80);
                setLabelColor(lblWeb, System.Drawing.Color.Green);
                setLabelText(lblWeb, "V pořádku");
                tcp.Close();
                portWeb = true;
            }
            catch (Exception)
            {
                setLabelColor(lblWeb, System.Drawing.Color.Red);
                setLabelText(lblWeb, "Chyba");
            }

        }

        private void btnSetupExe_Click(object sender, EventArgs e)
        {
            if (!File.Exists("Setup.exe")) MessageBox.Show("Chyba! Soubor 'setup.exe' nebyl nalezen v adresáři hry.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else Process.Start("Setup.exe");
        }

        private void btnPortTest_MouseEnter(object sender, EventArgs e)
        {
            tbInfo2.Text = "Ozkouší, jestli se z tohoto počítače dá připojit na porty serveru které používá hra a webové stránky.";
        }

        private void rbGrEfGW_Click(object sender, EventArgs e)
        {
            if (rbGrEfGW.Checked == true)
            {
                if (MessageBox.Show("Pokud budeš chtít v budoucnu GW efekty odstranit, tak to půjde jen stažením všech patchů od začátku. Chceš pokračovat?", "Opravdu?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    rbGrEfGW.Checked = false;
                    return;
                }
            }
        }

        private void rbGrEfGW_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Nastaví upravené efekty optimalizované pro Guild Wars.";
        }

        private void rbGrEfStandard_Click(object sender, EventArgs e)
        {
            if (settings[SET_EFF] == SET_EFF_GW)
            {
                MessageBox.Show("Jelikož používáš GW efekty, musíš nejdříve resetovat patche (hledej na další záložce).", "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rbGrEfGW.Checked = true;
            }
        }

        private void rbGrEfOsek_Click(object sender, EventArgs e)
        {
            if (settings[SET_EFF] == SET_EFF_GW)
            {
                MessageBox.Show("Jelikož používáš GW efekty, musíš nejdříve resetovat patche (hledej na další záložce).", "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rbGrEfGW.Checked = true;
            }
        }

        private void antishake_MouseEnter(object sender, EventArgs e)
        {
            tbInfo.Text = "Toto nastavení dokáže vypnout veškeré třesení obrazu ve hře.";
        }

        private void antishake_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbScreenWindow_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbScreenFullScreen_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
