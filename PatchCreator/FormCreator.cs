﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Collections;


/* atributy patche:
 * number="1"
 * type="rewrite"
 * date="2. květen 2009"
 * file="reborn.grf"
 * patch_file="2009-04-25RebornBase.grf"
 * author="Koca"
 * text="Zakladni soubor reborn.grf"
 * caption = "Zakladni Soubor"
 */

namespace PatchManager
{
    public partial class FormCreator : Form
    {
        public const int
            PATCH_INVALID = 0,
            PATCH_REWRITE = 1,
            PATCH_DELETE = 2,
            PATCH_PATCH = 3,
            PATCH_TEXT = 4;

        struct PatchInfo
        {
            public int num, type;
            public string patch_file, dest_file, type_str, author, text, caption;
            public DateTime date;
            public bool hide, disable;
        }
        List<PatchInfo> patches;
        public String version = "1";
        public bool changes = false;

        public FormCreator()
        {
            InitializeComponent();
            if (File.Exists("!patch.xml")) LoadPatchFile("!patch.xml");
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            LoadPatchFile(openFileDialog1.FileName);

        }

        private int typeToInt(String type)
        {
            switch (type)
            {
                case "rewrite":
                    return PATCH_REWRITE;
                case "delete":
                    return PATCH_DELETE;
                case "patch":
                    return PATCH_PATCH;
                case "text":
                    return PATCH_TEXT;
                default:
                    return PATCH_INVALID;
            }
        }

        private String typeToStr(int type)
        {
            switch (type)
            {
                case PATCH_REWRITE:
                    return "rewrite";
                case PATCH_DELETE:
                    return "delete";
                case PATCH_PATCH:
                    return "patch";
                case PATCH_TEXT:
                    return "text";
                default:
                    return "invalid";
            }

        }

        private void listPatchu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (patches == null) return;
            if(listPatchu.SelectedIndex >= patches.Count || listPatchu.SelectedIndex < 0) return;

            PatchInfo pi = patches[listPatchu.SelectedIndex];

            tbPatchNum.Text = pi.num.ToString();
            if (pi.type != PATCH_INVALID) comboPatchType.SelectedIndex = pi.type;
            else comboPatchType.SelectedIndex = 0;

            tbPatchDateDay.Text = pi.date.Day.ToString();
            tbPatchDateMonth.Text = pi.date.Month.ToString();
            tbPatchDateYear.Text = pi.date.Year.ToString();
            tbPatchDestFile.Text = pi.dest_file;
            tbPatchAuthor.Text = pi.author;
            tbPatchText.Text = pi.text;
            tbPatchCaption.Text = pi.caption;
            switch (pi.type)
            {
                case PATCH_DELETE:
                    tbPatchDestFile.Enabled = true;    
                    tbPatchPatchFile.Text = "";
                    tbPatchPatchFile.Enabled = false;
                    break;
                case PATCH_TEXT:
                    tbPatchDestFile.Text = "";
                    tbPatchDestFile.Enabled = false;
                    tbPatchPatchFile.Text = "";
                    tbPatchPatchFile.Enabled = false;
                    break;
                default:
                    tbPatchDestFile.Enabled = true;
                    tbPatchPatchFile.Enabled = true;
                    tbPatchPatchFile.Text = pi.patch_file;
                    break;
            }
            chbHidePatch.Checked = pi.hide;
            chbDisablePatch.Checked = pi.disable;

            while (listPatchu.Items.Count > patches.Count)
            {
                listPatchu.Items.RemoveAt(listPatchu.Items.Count - 1);
            }

        }

        private void btnNewPatch_Click(object sender, EventArgs e)
        {
            int new_num = 1;
            if(patches != null) new_num = patches[patches.Count-1].num+1;
            listPatchu.Items.Add(new_num + " - Novy patch");
            listPatchu.SelectedIndex = listPatchu.Items.Count - 1;

            tbPatchNum.Text = new_num.ToString();
            comboPatchType.SelectedIndex = PATCH_PATCH;
            tbPatchDateDay.Text = DateTime.Now.Day.ToString();
            tbPatchDateMonth.Text = DateTime.Now.Month.ToString();
            tbPatchDateYear.Text = DateTime.Now.Year.ToString();
            tbPatchDestFile.Text = "reborn.grf";
            tbPatchPatchFile.Text = "";
            tbPatchCaption.Text = "";
            tbPatchText.Text = "";
        }

        private void comboPatchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboPatchType.SelectedIndex)
            {
                case PATCH_DELETE:
                    tbPatchDestFile.Enabled = true;
                    tbPatchPatchFile.Enabled = false;
                    btnOK.Enabled = true;
                    break;
                case PATCH_INVALID:
                    btnOK.Enabled = false;
                    break;
                case PATCH_TEXT:
                    tbPatchDestFile.Enabled = false;
                    tbPatchPatchFile.Enabled = false;
                    break;
                default:
                    tbPatchDestFile.Enabled = true;
                    tbPatchPatchFile.Enabled = true;
                    btnOK.Enabled = true;
                    break;
            }
        }

        private void btnDelPatch_Click(object sender, EventArgs e)
        {
            if (patches == null || listPatchu.Items.Count == 0 || listPatchu.SelectedIndex >= patches.Count || listPatchu.SelectedIndex < 0 ) return;

            try
            {
                PatchInfo pi = patches[listPatchu.SelectedIndex];
                if (MessageBox.Show("Chcete smazat patch " + pi.num + " - " + pi.type_str + " " + pi.dest_file + "?", "Potvrdit smazani", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes) return;
                patches.RemoveAt(listPatchu.SelectedIndex);
                listPatchu.Items.RemoveAt(listPatchu.SelectedIndex);
                listPatchu.SelectedIndex = -1;
                tbPatchAuthor.Text = "";
                tbPatchCaption.Text = "";
                tbPatchDateDay.Text = "";
                tbPatchDestFile.Text = "";
                tbPatchNum.Text = "";
                tbPatchPatchFile.Text = "";
                tbPatchText.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadPatchFile(String filename)
        {
            try
            {
                XmlTextReader xml = new XmlTextReader(filename);
                patches = new List<PatchInfo>();
                while (xml.Read())
                {
                    switch (xml.NodeType)
                    {
                        case XmlNodeType.Element:
                            switch (xml.Name.ToLower())
                            {
                                case "patch_definition":
                                    while (xml.MoveToNextAttribute())
                                    {
                                        if (xml.Name.ToLower().Equals("version"))
                                        {
                                            version = xml.Value;
                                            tbVersion.Text = version;
                                        }
                                    }
                                    break;
                                case "patch":
                                    PatchInfo pi = new PatchInfo();
                                    while (xml.MoveToNextAttribute())
                                    {
                                        switch (xml.Name.ToLower())
                                        {
                                            case "number":
                                                if (!int.TryParse(xml.Value, out pi.num))
                                                {
                                                    MessageBox.Show("Spatny nazev cisla " + xml.Value + " na radku " + xml.LineNumber, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    return;
                                                }
                                                break;
                                            case "type":
                                                pi.type = typeToInt(xml.Value.ToLower());
                                                pi.type_str = typeToStr(pi.type);
                                                break;
                                            case "patch_file":
                                                pi.patch_file = xml.Value;
                                                break;
                                            case "file":
                                                pi.dest_file = xml.Value;
                                                break;
                                            case "date":
                                                try
                                                {
                                                    pi.date = DateTime.ParseExact(xml.Value, "d. M. yyyy", null);
                                                }
                                                catch (Exception)
                                                {
                                                    pi.date = DateTime.ParseExact("1. 1. 1970", "d. M. yyyy", null);
                                                }
                                                break;
                                            case "author":
                                                pi.author = xml.Value;
                                                break;
                                            case "text":
                                                pi.text = xml.Value;
                                                break;
                                            case "caption":
                                                pi.caption = xml.Value;
                                                break;
                                            case "noshow":
                                                if (xml.Value.Equals("true")) pi.hide = true;
                                                break;
                                            case "disable":
                                                if (xml.Value.Equals("true")) pi.disable = true;
                                                break;
                                            //default:
                                            //    MessageBox.Show("Neznámý atribut v definici patchu. Řádek: " + xml.LineNumber + "; sloupec: " + xml.LinePosition, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            //    return;
                                        }
                                    }

                                    if (pi.num <= 0 || (pi.dest_file == null && pi.type != PATCH_TEXT ) ||
                                        ((pi.type == PATCH_REWRITE || pi.type == PATCH_PATCH) && pi.patch_file == null))
                                    {
                                        MessageBox.Show("Chybná definice patche. Řádek: " + xml.LineNumber, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                    patches.Add(pi);
                                    break;
                            }
                            break;
                    }

                }
                xml.Close();
                refreshList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshList()
        {
            if (patches == null) return;
            listPatchu.Items.Clear();
            foreach (PatchInfo pi in patches)
            {
                listPatchu.Items.Add(pi.num + " - " + pi.type_str + " " + pi.dest_file + ": " + pi.caption + (pi.hide ? " [h]" : "") + (pi.disable ? " [d]" : ""));
            }
        }

        private void refreshList(int selected)
        {
            refreshList();
            if(selected < listPatchu.Items.Count && selected >= 0) listPatchu.SelectedIndex = selected;
        }

        private void btnSavePatch_Click(object sender, EventArgs e)
        {
            if (savePatchFile() == 0)
            {
                String fn = openFileDialog1.FileName.Equals("") ? "!patch.xml" : openFileDialog1.FileName;
                MessageBox.Show("Soubor " + fn + " uložen.", "Patch uložen", MessageBoxButtons.OK, MessageBoxIcon.Information);
                changes = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (listPatchu.SelectedIndex == -1) return;
            int patchnum = -1;
            if (!int.TryParse(tbPatchNum.Text, out patchnum) || patchnum <= 0)
            {
                MessageBox.Show("Špatně vyplněné číslo patche.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DateTime dt;
            if (!DateTime.TryParseExact(tbPatchDateDay.Text+"."+tbPatchDateMonth.Text+"."+tbPatchDateYear.Text, "d.M.yyyy", null, System.Globalization.DateTimeStyles.None, out dt))
            {
                MessageBox.Show("Špatně vyplněný datum patche. Musí být ve formátu \"d. m. yyyy\"", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (comboPatchType.SelectedIndex <= PATCH_INVALID)
            {
                MessageBox.Show("Vybraný patch nelze použít", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            switch (comboPatchType.SelectedIndex)
            {
                case PATCH_DELETE:
                    if (tbPatchDestFile.Text.Equals(""))
                    {
                        MessageBox.Show("Cílový soubor musí být vyplněný", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case PATCH_PATCH:
                case PATCH_REWRITE:
                    if (tbPatchDestFile.Text.Equals(""))
                    {
                        MessageBox.Show("Cílový soubor musí být vyplněný", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (tbPatchPatchFile.Text.Equals(""))
                    {
                        MessageBox.Show("Patch soubor musí být vyplněný", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
                case PATCH_INVALID:
                    MessageBox.Show("Nelze použít patch typ \"invalid.\"", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            PatchInfo pi = new PatchInfo();
            pi.num = patchnum;
            pi.date = dt;
            pi.type = comboPatchType.SelectedIndex;
            pi.author = tbPatchAuthor.Text;
            pi.caption = tbPatchCaption.Text;
            pi.text = tbPatchText.Text;
            pi.type_str = typeToStr(pi.type);
            switch (pi.type)
            {
                case PATCH_PATCH:
                case PATCH_REWRITE:
                    pi.dest_file = tbPatchDestFile.Text;
                    pi.patch_file = tbPatchPatchFile.Text;
                    break;
                case PATCH_DELETE:
                    pi.dest_file = tbPatchDestFile.Text;
                    break;
            }
            pi.hide = chbHidePatch.Checked;
            pi.disable = chbDisablePatch.Checked;

            if (patches == null) patches = new List<PatchInfo>();
            if (listPatchu.Items.Count > patches.Count) patches.Add(pi);
            else patches[listPatchu.SelectedIndex] = pi;
            changes = true;
            refreshList(listPatchu.SelectedIndex);
            MessageBox.Show("Patch uložen.", "Patch ulozen", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnPatchTypeHelp_Click(object sender, EventArgs e)
        {
            String message = "";
            switch (comboPatchType.SelectedIndex)
            {
                case PATCH_INVALID:
                    message = "Typ \"invalid\" slouží pouze pro indikaci, že je něco špatně. Nelze použít.";
                    break;
                case PATCH_DELETE:
                    message = "Typ patche \"delete\" slouží pro mazání souborů.\n"+
                        "Vložte jméno cílového souboru, který chcete smazat.";
                    break;
                case PATCH_REWRITE:
                    message = "Typ \"rewrite\" slouží ke stáhnutí souboru.\n"+
                        "Dobré při stahování různých souborů nebo exe.\n"+
                        "Patch soubor je jméno souboru na serveru, Cílový soubor je jméno souboru u klienta.";
                    break;
                case PATCH_PATCH:
                    message = "Typ \"patch\" slouží k patchování GRF souboru.\n" +
                        "Patch soubor je jméno GRF archivu s patchem na serveru,\n" +
                        "cílový soubor je jméno GRF archivu, ktetý chcete opatchovat.";
                    break;
                case PATCH_TEXT:
                    message = "Typ \"text\" slouží k prostému zobrazení zprávy v okně patcheru.\n" +
                        "Hodí se, pokud chcete vypsat důležitou informaci, ale nechcete provádět žádné úpravy klienta.";
                    break;
            }
            if(message.Equals("")) return;
            MessageBox.Show(message,"Informace o typu patche",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

        private void tbPatchDateDay_TextChanged(object sender, EventArgs e)
        {
            if (!tbPatchDateDay.Focused) return;
            if (tbPatchDateDay.Text.Length >= 2) tbPatchDateMonth.Focus();
            if(tbPatchDateDay.Text.EndsWith("."))
            {
                tbPatchDateDay.Text = tbPatchDateDay.Text.Substring(0, tbPatchDateDay.Text.Length - 1);
                tbPatchDateMonth.Focus();
            }
        }

        private void tbPatchDateMonth_TextChanged(object sender, EventArgs e)
        {
            if (!tbPatchDateMonth.Focused) return;
            if (tbPatchDateMonth.Text.Length >= 2 || (!tbPatchDateMonth.Text.Equals("1") && !tbPatchDateMonth.Text.Equals(""))) tbPatchDateYear.Focus();
            if (tbPatchDateMonth.Text.EndsWith("."))
            {
                tbPatchDateMonth.Text = tbPatchDateMonth.Text.Substring(0, tbPatchDateMonth.Text.Length - 1);
                tbPatchDateYear.Focus();
            }
        }

        private int savePatchFile()
        {
            if (patches == null)
            {
                MessageBox.Show("Nejsou načteny žádné patche.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            try
            {
                foreach (PatchInfo pi in patches)
                {
                    if (pi.type == PATCH_INVALID)
                    {
                        MessageBox.Show("Patch " + pi.num + " je typu invalid. Je potřeba toto opravit.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 1;
                    }
                }
                string ver = tbVersion.Text;
                String fn = openFileDialog1.FileName;
                if (fn.Equals("")) fn = "!patch.xml";
                XmlTextWriter wr = new XmlTextWriter(fn, Encoding.UTF8);
                wr.WriteStartDocument();
                wr.WriteWhitespace("\n");
                wr.WriteStartElement("patch_definition");
                wr.WriteAttributeString("version", ver);
                foreach (PatchInfo pi in patches)
                {
                    wr.WriteWhitespace("\n\t");
                    wr.WriteStartElement("patch");
                    wr.WriteAttributeString("number", pi.num.ToString());
                    if (pi.type_str != null) wr.WriteAttributeString("type", pi.type_str);
                    wr.WriteAttributeString("date", pi.date.ToString("d. M. yyyy"));
                    if (pi.dest_file != null) wr.WriteAttributeString("file", pi.dest_file);
                    if (pi.patch_file != null) wr.WriteAttributeString("patch_file", pi.patch_file);
                    if (pi.author != null) wr.WriteAttributeString("author", pi.author);
                    if (pi.caption != null) wr.WriteAttributeString("caption", pi.caption);
                    if (pi.text != null) wr.WriteAttributeString("text", pi.text);
                    if (pi.hide == true) wr.WriteAttributeString("noshow", "true");
                    if (pi.disable == true) wr.WriteAttributeString("disable", "true");
                    wr.WriteEndElement();
                }
                wr.WriteWhitespace("\n");
                wr.WriteEndElement();
                wr.WriteEndDocument();
                wr.Close();
                return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
            
        }

        private void FormCreator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(changes)
                switch (MessageBox.Show("Byly provedeny neuložené změny. Chcete uložit soubor?", "Změny", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        if (savePatchFile() == 1) e.Cancel = true;
                        break;
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
        }
    }
}
